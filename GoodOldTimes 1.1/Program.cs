﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodOldTimes_1._1
{
    class Program
    {
        static void Main(string[] args)
        {
            var data = new dynamic[30];
            data[0] = File.ReadAllText(@"c:\file.txt", Encoding.UTF8).Split(',');
            FileStream wordFreqWrite;
            StreamReader wordFreqRead;
            var lines = File.OpenText(@"c:\text1.txt");
            data[8] = 0;
            while (true)
            {
                data[1] = lines.ReadLine();
                if (data[1] == null)
                {
                    break;
                }
                data[2] = -1;
                data[3] = 0;
                foreach (var c in (string)data[1])
                {
                    if (data[2] == -1)
                    {
                        if (Char.IsLetterOrDigit(c))
                        {
                            data[2] = data[3];
                        }
                    }
                    else
                    {
                        if (!Char.IsLetterOrDigit(c))
                        {
                            data[10] = 0;
                            data[4] = false;
                            data[5] = ((string)data[1]).Substring(data[2], data[3] - data[2]);
                            data[2] = -1;
                            data[5] = ((string)data[5]).Replace(" ", "");
                            if (((string)data[5]).Length > 1 && ((String[])data[0]).FirstOrDefault(stopWord => stopWord.Equals(data[5])) == null)
                            {
                                wordFreqRead = new StreamReader(@"d:\freq.txt");

                                while (true)
                                {
                                    data[6] = wordFreqRead.ReadLine();
                                    if (data[6] == null)
                                    {
                                        break;
                                    }
                                    data[6] = ((string)data[6]).Replace(" ", "");
                                    data[7] = double.Parse(((String)data[6]).Split(',')[1]);
                                    data[6] = ((String)data[6]).Split(',')[0].Replace(" ", "");
                                    if (((String)data[6]).Equals((String)data[5]))
                                    {
                                        data[7]++;
                                        data[4] = true;

                                        break;
                                    }
                                    data[10]++;
                                }
                                wordFreqRead.Close();
                                wordFreqWrite = File.OpenWrite(@"d:\freq.txt");
                                if (data[4] == true)
                                {
                                    data[9] = new UTF8Encoding(true).GetBytes(string.Format("{0},{1:n4}", ((String)data[5]).PadRight(18, ' '), data[7]) + Environment.NewLine);

                                    wordFreqWrite.Seek(-27 * (data[8] - data[10]), SeekOrigin.End);
                                    wordFreqWrite.Write(((byte[])data[9]), 0, ((byte[])data[9]).Length);
                                }
                                else
                                {
                                    data[8]++;
                                    data[9] = new UTF8Encoding(true).GetBytes(string.Format("{0},{1:n4}", ((String)data[5]).PadRight(18, ' '), 1) + Environment.NewLine);
                                    wordFreqWrite.Seek(0, SeekOrigin.End);
                                    wordFreqWrite.Write(((byte[])data[9]), 0, ((byte[])data[9]).Length);
                                    wordFreqWrite.Flush();

                                }
                                wordFreqWrite.Close();
                            }

                        }
                    }
                    data[3]++;
                }


            }
            data = new dynamic[30];
            wordFreqRead = new StreamReader(@"d:\freq.txt");
            while (true)
            {
                data[25] = wordFreqRead.ReadLine();
                
                if (data[25] == null)
                {
                    break;
                }
                data[25] = ((string)data[25]).Replace(" ", "");
                data[26] = double.Parse(((String)data[25]).Split(',')[1]);
                data[25] = ((String)data[25]).Split(',')[0];
                data[27] = "";
                for (int i = 0; i < 25; i++)
                {
                    if (data[i] == null || double.Parse(((String)data[i]).Split(',')[1]) < data[26])
                    {
                        data[27] = data[25] + "," + data[26];
                        for (int j = i; j < 24; j++)
                        {
                            data[28] = data[j];
                            data[j] = data[27];
                            if (data[j] == null)
                            {
                                break;
                            }
                            data[27] = data[28];
                        }
                        break;
                    }
                }


            }
            for (int i = 0; i < 25; i++)
            {
                if (data[i] == null)
                {
                    break;
                }
                Console.WriteLine(((String)data[i]).Split(',')[0] + "-" + double.Parse(((String)data[i]).Split(',')[1]));

            }
        }
    }
}
